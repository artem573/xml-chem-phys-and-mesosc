#! /usr/bin/python
# -*- coding: utf-8 -*-

# The program for xml generation of Chemical Physics and Mesoscopy journal
# By Shaklein Artem
# 2015

from jinja2 import Environment, PackageLoader
import re
from os import listdir
from os.path import isfile,join
from datetime import datetime


class ObjectDict(dict):
    """Makes a dictionary behave like an object."""
    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)
        
    def __setattr__(self, name, value):
        self[name] = value

def remove_newline(line):
    if line[-1] == '\n': line = line[:-1]

def check(arg):
    if not arg:
        raise ValueError('A keyword is missing')
    else:
        return arg

      
def decode(obj):
    def decode_list(l):
        for i in range(len(l)):
            if isinstance(l[i], dict):
                decode_dict(l[i])
            else:
                if isinstance(l[i], list):
                    decode_list(l[i])
                else:
                    try:
                        l[i] = l[i].decode('utf-8')
                    except:
                        print l[i]
                        raise(ValueError)

    def decode_dict(obj):
        for key in obj:
            if isinstance(obj[key], dict):
                decode_dict(obj[key])
            else:
                if isinstance(obj[key], list):
                    decode_list(obj[key])
                else:
                    try:
                        obj[key] = obj[key].decode('utf-8')
                    except:
                        print obj[key]
                        raise(ValueError)

    if isinstance(obj, dict):
      decode_dict(obj)
    elif isinstance(obj, list):
        decode_list(obj)


        
def main():

    env = Environment(loader=PackageLoader('xml_jinja', './'))
    env.filters['check'] = check
    template = env.get_template('template_ChPhM_elibrary')

    art_dict = ObjectDict(udk='', title_ru='', summary_ru='', keywords_ru=[], title_en='', summary_en='', keywords_en=[], text='', financing='', biblio=[])
    auth_dict = ObjectDict(surname_ru='', initials_ru='', first_name_ru='', last_name_ru='', orgName_ru='', surname_en='', initials_en='', orgName_en='', email='', work_number=[], index='')
    journal = ObjectDict()

    toc = []

    # TOC pagination
    with open('Содержание.txt') as f_toc:
        line = f_toc.readline()
        line = line.replace('\xef\xbb\xbf','')
        if line.count('СОДЕРЖАНИЕ') == 0:
            raise ValueError('There is no keyword "СОДЕРЖАНИЕ" in the first line of the toc file given')
        line = f_toc.next()
        while line.isspace():
            line = f_toc.next()
        while line.count('РЕФЕРАТЫ') == 0:
            # Pass article title
            while not line.isspace():
                line = f_toc.next()
            while line.isspace():
                line = f_toc.next()
            toc.append(int(float(line)))
            line = f_toc.next()
            while line.isspace():
                line = f_toc.next()
        line = f_toc.next()
        while line.isspace():
            line = f_toc.next()
        toc.append(int(float(line)))

    # Search for *.txt files
    articles_path = [f for f in listdir('./') if isfile(join('./', f)) and '.txt' in f and not 'Содержание' in f and not '_repl' in f and not '~' in f and not '#' in f]

    
    for path_i in range(len(articles_path)):
        for path_j in range(len(articles_path)):
            counter = 0
            line_i = articles_path[path_i]
            line_j = articles_path[path_j]
            if line_i < line_j:
                articles_path[path_i] = line_j
                articles_path[path_j] = line_i

    # Search for *.pdf files
    pdf_files = ['']*len(articles_path)
    for f in listdir('./'):
        if isfile(join('./', f)) and f.count('.pdf') == 1:
            for counter in range(len(toc)):
                if f.count(str(toc[counter])) == 1 and not f.count(' '):
                    pdf_files[counter] = f


    with open('chphm_unicode.xml', 'w+') as f_journal:
        journal.title = 'Химическая физика и мезоскопия'
    #    title = title.decode('utf-8')
        journal.volume = '19'
        journal.issue = '4'
        journal.year = '2017'
        journal.start_page = str(toc[0]-2)
        journal.end_page = str(toc[-1]+2)
        journal.articles_count = str(len(articles_path))
        journal.date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        journal.date_short = datetime.now().strftime("%Y-%m-%d")
        
        # Replace nonbreak spaces
        for path_i in range(len(articles_path)):
            content = ''
            with open(articles_path[path_i], 'r') as f_replace:
                for line in f_replace:
                    line = line.replace('\xc2\xa0', ' ').replace('  ', ' ').replace('\r', '').replace('\xef\x81\xaa',' ').replace('\xef\x81\xa1',' ').replace('\xef\x81\xb2',' ').replace('\xef\x81\xb3',' ').replace('\xef\x81\xae',' ').replace('\xef\x82\xb4',' ').replace('	',' ')

                    line = line.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;')

                    # Replace special characters by latex equivalent
                    line = line.replace('\xef\x80\xa0', '$\nu$')
                    line = line.replace('\xef\x82\xab', '$\leftrightarrow$')
                    line = line.replace('\xef\x82\xbb', '$\simeq$')
                    line = line.replace('\xef\x83\x97', '$\cdot$')
                    line = line.replace('\xef\x81\xa7', '\gamma')
                    line = line.replace('\xef\x81\xa4', '$\delta$')
                    line = line.replace('\xef\x81\xb5', '$\upsilon$')
                    line = line.replace('\xef\x81\xa8', '$\eta$')

                    line = line.replace('\xef\x81\xa5', '$\epsilon$')
                    line = line.replace('\xef\x82\xb8', '$\div$')
                    line = line.replace('\xef\x81\xad', '$\mu$')
                    line = line.replace('\xef\x82\xb0', '$^{\circ}$')
                    line = line.replace('\xef\x82\xad', '$\updownarrows$')
                    line = line.replace('\xef\x82\xaf', '$\uparrow$')
                    line = line.replace('\xef\x82\xae', '$\rightarrow$')

                    
                    if line[0] == ' ':
                        line = line[1:]
                    content += line
            f_replace.close()

            articles_path[path_i] = (articles_path[path_i][:-4].decode('utf-8')+'_repl.txt'.decode('utf-8')).encode('utf-8')

            with open(articles_path[path_i], 'w') as f_replace:
                for item in content:
                    f_replace.write(item)
            f_replace.close()

        articles = []
        article_counter = 0
        for path_i in range(len(articles_path)):
            articles.append(ObjectDict(art_dict))
            article = articles[-1]
            article_counter += 1

            print articles_path[path_i]

            with open(articles_path[path_i]) as f_chphm:
                # UDK
                line = f_chphm.readline()
                line = line.replace('\xef\xbb\xbf','')
                if(line.count('УДК')):
                    article.udk = line.replace('\n','').split('УДК ',1)[1]

                for line in f_chphm:
                    if not line.isspace():
                        break

                # Title_ru
                while not line.isspace():
                    article.title_ru += line.replace('\n',' ').decode('utf-8').lower().encode('utf-8')
                    line = f_chphm.next()

                temp = article.title_ru.decode('utf-8')
                temp = '. '.join(temp_i.capitalize() for temp_i in temp.split('. '))
                article.title_ru = temp.encode('utf-8')

                while line.isspace():
                    line = f_chphm.next()

                temp = 'a'
                while not temp.isspace():
                    temp = f_chphm.next()
                    line += temp

                line = line.replace('\n', ' ')

                # Authors_ru
                next_author = True
                single_work = True
                authors = []
                while next_author:
                    authors.append(ObjectDict(auth_dict))
                    authors[-1].index = str(len(authors)).zfill(3)
                    author_fam = line.split(' ',1)[0]
                    line = line.split(' ',1)[1]
                    author_io = line.split(' ',1)[0]
                    line = line.split(' ',1)[1]
                    author_io += line.split(' ',1)[0]

                    author_work = []
                    while re.findall('\d{1}',author_fam):
                        temp = re.findall('\d{1}',author_fam)
                        single_work = False
                        author_work.append(temp[0])
                        author_fam = author_fam.split(author_work[-1],1)[1]
                        if(author_fam[0] == ','):
                            author_fam = author_fam.split(',',1)[1]

                    author_fam = author_fam.decode('utf-8')[0].upper() + author_fam.decode('utf-8')[1:].lower()
                    author_fam = author_fam.encode('utf-8')

                    if author_io.count(',') > 0:
                        line = line.split(' ',1)[1]
                        author_io = author_io.split(',',1)[0]
                    else:
                        next_author = False

                    authors[-1].surname_ru = author_fam
                    authors[-1].initials_ru = author_io
                    authors[-1].work_number = author_work

                # Affiliation_ru
                for line in f_chphm:
                    if line.count('___'):
                        break
                    if not line.isspace() and not line[:2].isdigit():
                        if line.count(','):
                            line = line.split(',',1)[0]
                        for author_i in range(len(authors)):
                            if single_work and not authors[author_i].orgName_ru:
                                authors[author_i].orgName_ru = line
                            else:
                                for work_i in range(len(authors[author_i].work_number)):
                                    if authors[author_i].work_number[work_i] == line[0]:
                                        if authors[author_i].orgName_ru:
                                            authors[author_i].orgName_ru += '; '
                                        authors[author_i].orgName_ru += line[1:]

                # Summary_ru
                for line in f_chphm:
                    if not line.isspace():
                        break

                if not line.decode('utf-8').lower().encode('utf-8').count('аннотация. '):
                    raise ValueError('There is no keyword "АННОТАЦИЯ. "')
                remove_newline(line)
                article.summary_ru = line.split('. ',1)[1]

                # Keywords_ru
                for line in f_chphm:
                    if not line.isspace():
                        break

                if not line.decode('utf-8').lower().encode('utf-8').count('ключевые слова: '):
                    print line
                    raise ValueError('There is no keyword "КЛЮЧЕВЫЕ СЛОВА: "')
                keywords_ru = []
                keywords = line.split(': ',1)[1]
                while keywords.count(','):
                    keywords_ru.append(keywords.split(', ',1)[0])
                    keywords = keywords.split(', ',1)[1]
                keywords = keywords.replace('\n','').replace('.','')
                if keywords[-1] == ' ':
                    keywords = keywords[:-1]
                keywords_ru.append(keywords)
                article.keywords_ru = keywords_ru

                for line in f_chphm:
                    if line.count('___'):
                        break


                # Main text and financing
                line = f_chphm.next()
                remove_newline(line)
                article.text = line

                key_financing = 0
                for line in f_chphm:
                    if line.decode('utf-8').lower().encode('utf-8').count('список литературы') or line.decode('utf-8').lower().encode('utf-8').count('список используемой литературы'):
                        break

                    if key_financing:
                        if line.isspace():
                            key_financing = 0
                        else:
                            article.financing += line

                    if line.count('Исследование выполнено за счет') or line.count('Исследование выполнено при ') or line.count('Работа выполнена при ') or line.count('Работа выполнена в рамках ') or line.count('Работа поддержана '):
                        key_financing = 1
                        article.financing = line

                    if key_financing == 0:
                        article.text += line

                article.financing = article.financing.replace('\n',' ')
                
                # Bibliography
                for line in f_chphm:
                    if not line.isspace():
                        break

                biblio = []
                line = line.replace('\n','')
                biblio.append(line.split('1. ',1)[1])
                for line in f_chphm:
                    if line.count('__'):
                        break

                    if not line.isspace():
                        line = line.replace('\n','')
                        number = str(len(biblio)+1)
                        if line.split()[0] == number + '.':
                            biblio.append(line.split(number+'. ',1)[1])
                        else:
                            biblio[-1] += line

                article.biblio = biblio

                # Title_en
                for line in f_chphm:
                    if not line.isspace():
                        break

                while not line.isspace():
                    line = line.replace('\n',' ')
                    article.title_en += line.lower()
                    line = f_chphm.next()

                temp = article.title_en
                temp = '. '.join(temp_i.capitalize() for temp_i in temp.split('. '))
                article.title_en = temp

                # Authors_en
                while line.isspace():
                    line = f_chphm.next()

                for author_i in range(len(authors)):
                    author_fam = line.split(' ',1)[0]
                    line = line.split(' ',1)[1]
                    author_io = line.split(' ',1)[0]
                    line = line.split(' ',1)[1]
                    author_io += line.split(' ',1)[0]
                    author_io = author_io.replace('\n','')
                    author_io = author_io.replace(' ','')

                    if author_io.count(',') > 0:
                        line = line.split(' ',1)[1]
                        author_io = author_io.split(',',1)[0]
#                        line = line.split(' ',1)[1]
#                        author_io += author_io.split(' ',1)[0]

                    for work_i in range(len(authors[author_i].work_number)):
                        author_fam = author_fam.split(authors[author_i].work_number[work_i],1)[1]
                        if(author_fam.count(',') > 0):
                            author_fam = author_fam.split(',',1)[1]

                    authors[author_i].surname_en = author_fam
                    authors[author_i].initials_en = author_io

                for line in f_chphm:
                    if not line.isspace():
                        break

                # Affiliation_en
                while not line.isspace():
                    if line.lower().count('academy of science'):
                        line = line.split('cience',1)[0] + 'ciences'
                    else:
                        if line.count(','):
                            line = line.split(',',1)[0]

                    for author_i in range(len(authors)):
                        if single_work and not authors[author_i].orgName_en:
                            authors[author_i].orgName_en = line
                        else:
                            for work_i in range(len(authors[author_i].work_number)):
                                if authors[author_i].work_number[work_i] == line[0]:
                                    if(authors[author_i].orgName_en != ''):
                                        authors[author_i].orgName_en += '; '
                                    authors[author_i].orgName_en += line[1:]

                    line = f_chphm.next()

                # Summary_en
                for line in f_chphm:
                    if not line.isspace():
                        break
                if(line.lower().count('summary. ') != 1):
                    raise ValueError('There is no keyword "SUMMARY. "')
                remove_newline(line)
                article.summary_en = line.split('. ',1)[1]
                for line in f_chphm:
                    if line.isspace(): break
                    else: article.summary_en += line

                # Keywords_en
                for line in f_chphm:
                    if not line.isspace():
                        break

                key_keywords = False
                if line.decode('utf-8').lower().encode('utf-8').count('words: ') == 0:
                    print line
                    raise ValueError('There is no keyword "KEYWORDS: "')
                keywords_en = []
                keywords = line.split(': ',1)[1]
                while keywords.count(',') > 0:
                    keywords_en.append(keywords.split(', ',1)[0])
                    keywords = keywords.split(', ',1)[1]
                keywords = keywords.replace('\n','')
                keywords = keywords.replace('.','')
                if keywords[-1] == ' ':
                    keywords = keywords[:-1]
                keywords_en.append(keywords)
                article.keywords_en = keywords_en

                # Rerefence_en
                for line in f_chphm:
                    if not line.isspace():
                        break
            
                if line.decode('utf-8').lower().encode('utf-8').count('references') == 0:
                    raise ValueError('There is no keyword "REFERENCES"')

                for line in f_chphm:
                    if ('___') in line:
                        break


                # Authors_ru full info
                for line in f_chphm:
                    if line.count('___') == 0 and not line.isspace():
                        break

                for author_i in range(len(authors)):
                    temp = line.split(',',1)[0]
                    authors[author_i].initials_ru = temp.split(authors[author_i].surname_ru + ' ',1)[1]
                    authors[author_i].first_name_ru, authors[author_i].last_name_ru = authors[author_i].initials_ru.split(' ',1)
                    if not line.count('mail'):
                        for line in f_chphm:
                            if line.isspace() or line.count('mail'):
                                break
                    if line.count('mail'):
                        line = line.replace('\n','').replace(' ','')
                        authors[author_i].email = line.split('mail:',1)[1]

                    for line in f_chphm:
                        if not line.isspace():
                            break

#                article.text = ''
                article.authors = authors
                article.start_page = str(toc[path_i])
                article.end_page = str(toc[path_i+1]-1)
                articles[-1] = article

                f_chphm.close()

            journal.articles = articles

    decode(journal)

    with open('chphm_unicode.xml', 'w+') as f_journal:
        f_journal.write(template.render(journal=journal).encode('utf-8'))

    print('Done')



if __name__ == '__main__':
    main()
