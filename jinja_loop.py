from datetime import datetime
from jinja2 import Environment, PackageLoader

def my_filter(arg):
    if not arg:
        raise ValueError('A keyword is missing')
    else:
        return arg
    
env = Environment(loader=PackageLoader('jinja_loop', './'))
env.filters['my_filter'] = my_filter

template = env.get_template('template_loop')

articles = []
for i in range(0,3):
    authors = []
    for j in range(0,2):
        i = str(i)
        an_author = dict(id=j, weight='50', height='170')
        authors.append(an_author)
    an_article = dict(id=i, authors=authors, pages='10')
    articles.append(an_article)
    
print template.render(the='the', go='goes', articles=articles)
    
with open('output','w+') as f_out:
    f_out.write(template.render(the='the', go='goes', article=articles))

d = dict(a='a')
d['b'] = 'b'
print d
