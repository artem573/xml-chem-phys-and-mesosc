from datetime import datetime
from jinja2 import Environment, PackageLoader

def my_filter(arg):
    if not arg:
        raise ValueError('A keyword is missing')
    else:
        return arg
    
env = Environment(loader=PackageLoader('testp', './'))
env.filters['my_filter'] = my_filter

template = env.get_template('mytemplate')

print template.render(the = 'the')
